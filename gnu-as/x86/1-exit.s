# Simply the program returns the exit status 0, indicating success
# to the kernel
.section .data
.section .text
.globl _start
_start:
 mov $1, %eax # exit system call.
 mov $0, %ebx # change this value and it will be displayed on the screen. 
 int $0x80
